
# update .git/ignore when you add programss here
# drop stamp for now to avoid coompiler warnings
PROGS = echo-server echo-client send sink

# Compiler flags
CFLAGS = -O1 -Wall -Wstrict-prototypes -Wmissing-prototypes

THREADS = -pthread

all: \
	$(PROGS)

clean:
	rm $(PROGS)


echo-server: echo-server.c
	cc $(CFLAGS) -g $(THREADS) -o echo-server echo-server.c

echo-client: echo-client.c
	cc $(CFLAGS) -g $(THREADS) -o echo-client echo-client.c

send: send.c
	cc $(CFLAGS) -g -o send send.c

sink: sink.c
	cc $(CFLAGS) -g $(THREADS) -o sink sink.c

stamp: stamp.c
	cc $(CFLAGS) -g -o stamp stamp.c


