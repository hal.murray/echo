# echo

These are some simple programs for generating network traffic.

They use port 8007 so you don't have to be root.
Firewall and/or iptables may return no-route (while ping and ssh work)

The echo client assumes that no packets will be lost.

echo-server takes 2 optional parameters
  the number of threads to start (default 1), and
  the amount of time to spin between recvfrom() and sendto() (default 0)
    the idea is to simulate work the server might do,
    for example ntpd's crypto for NTS
    units are microseconds
It will run until ^C-ed.

sink takes 1 optinal parameter
  the number of threads to start (default 1)
It will run until ^C-ed.

Both echo-server and sink will print out some statistics if you
poke <return> at them [1].  The numbers cover since the last time
they were printed.  If you want server statistics, the general
pattern is to:
  start the server,
  start the client with lots of packets so it will run for a long time
  poke <return> at the server to reset the statistics
  wait a while
  poke <return> at the server to get the recent statistics
Typical printout:
  Grabbed 259517 packets in 3.768 seconds
  Wall: 68872 packets/second, 14.5 uSec/pkt
  CPU: 0.069 usr, 0.383 sys, 12%, 1.7 uSec/pkt

------

1) You have to poke <return> because the tty is in line mode.

---------

SO_REUSEPORT allows multiple threads using separate sockets
to listen on the same port number.
On receive, the kernel hashes on the 4-tuple source/dest/address/port
to pick the socket that gets a packet.

The SO_REUSEPORT socket option
  https://lwn.net/Articles/542629/

