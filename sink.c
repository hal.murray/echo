/*  Last modified on Fri Jan  7 01:13:51 PST 2000 by murray  */


#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

int threads = 1;	/* first arg */

bool run = true;
bool debug = true;
int sock;

#define THREADS 10
int packets[THREADS];
pthread_t thread[THREADS];

int echoport = 8007;

static void bailout(char* msg)
{
  int saverrr = errno;
  char timetxt[100];
  time_t nowbin;
  struct tm nowstruct;
  time(&nowbin);
  localtime_r(&nowbin, &nowstruct);
  strftime(timetxt, sizeof(timetxt), "%Y-%b-%d %H:%M", &nowstruct);
  printf("** %s %s: errno = %d, %s\n", timetxt, msg, saverrr, strerror(saverrr));
  fflush(stdout);
  sleep(10);  /* Beware of loops in scripts. */
  exit(1);
}

static void catchINT(int sig, siginfo_t *si, void *u) {
  run = false;
}

static void setCatcher(void) {
  int ec;
  struct sigaction vec;

  memset(&vec, '\0', sizeof(vec));
  sigemptyset(&vec.sa_mask);
  vec.sa_sigaction = catchINT;
  vec.sa_flags = SA_SIGINFO;

  ec = sigaction(SIGINT, &vec, NULL);
  if (-1 == ec) bailout("sigaction");

}

static void* echoer (void *arg) {
  int *counter = (int*)arg;

  while (run) {
    uint8_t buffer[100000];
    ssize_t bytes;
    struct sockaddr clientAddr;
    socklen_t clientSize = sizeof(clientAddr);

    bytes = recvfrom(sock, &buffer, sizeof(buffer), 0, &clientAddr, &clientSize);
    if (bytes < 0) {
      if (!run) break;
      bailout("recvfrom");
    }

    *counter += 1;

  }
  return NULL;
}

int main (int argc, char *argv[])
{
 
  int seen[THREADS];
  struct timespec start, finish;
  struct timeval start_s, finish_s;
  struct timeval start_u, finish_u;
  struct rusage usage;
  double wall, system, user;
  struct sockaddr_in server;
  int ec;

  if (argc > 2) bailout("too many args");
  if (argc > 1) threads = atoi(argv[1]);
  if (threads < 1 || threads > THREADS) bailout("Bad threads");

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) bailout("socket");
    
  bzero((char *)&server, sizeof(server));
  server.sin_family      = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port        = htons(echoport);
  ec = bind(sock, (struct sockaddr *)&server, sizeof(server));
  if (ec < 0) bailout("bind");

  signal(SIGPIPE, SIG_IGN);
  setCatcher();
  if (!debug) {
    ec = daemon(1, 1);
    if (ec < 0) bailout("daemon");
  }

  printf("Sink: %d threads\n", threads);
  fflush(stdout);

  clock_gettime(CLOCK_REALTIME, &start);
  getrusage(RUSAGE_SELF, &usage);
  start_u = usage.ru_utime;
  start_s = usage.ru_stime;

  for (int i = 0; i<threads; i++) {
    packets[i] = 0;
    seen[i] = 0;
    pthread_create(&thread[i], NULL, echoer, &packets[i]);
  }

  while (run) {
    int pkts;
    char c;

    /* seems to start in readline mode -- use return */
    /* remote stuff doesn't signal on control-C */
    c = getchar();
    if (c == '\03') run = false;

    clock_gettime(CLOCK_REALTIME, &finish);
    getrusage(RUSAGE_SELF, &usage);
    finish_u = usage.ru_utime;
    finish_s = usage.ru_stime;
    wall = (finish.tv_sec-start.tv_sec) +
           (finish.tv_nsec-start.tv_nsec)/1E9;
    system = (finish_s.tv_sec-start_s.tv_sec) +
           (finish_s.tv_usec-start_s.tv_usec)/1E6;
    user = (finish_u.tv_sec-start_u.tv_sec) +
         (finish_u.tv_usec-start_u.tv_usec)/1E6;

    if (!run) printf("\n");  /* avoid ^C on screen */
    pkts = 0;
    for (int i=0; i<threads; i++) {
      int grab = packets[i];
      int new = grab-seen[i];
      if (threads > 1) printf("%3d: %8d\n", i, new);
      seen[i] = grab;
      pkts += new;
    }

    printf("Grabbed %d packets in %.3f seconds\n", pkts, wall);
    if (pkts) printf("Wall: %d packets/second, %.1f uSec/pkt\n",
        (int)(pkts/wall), wall*1E6/pkts);
    printf("CPU: %.3f usr, %.3f sys", user, system);
    printf(", %.0f%%", (user+system)*100/wall);
    if (pkts) printf(", %.1f uSec/pkt", (user+system)*1E6/pkts);
    printf("\n");
    fflush(stdout);

    start = finish;
    start_u = finish_u;
    start_s = finish_s;

  }

  close(sock);
  return 0;
}
