/* echo-server.c -- for testing network performance */

#define _GNU_SOURCE

#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

int threads = 1;	/* first arg */
int spin = 0;		/* second arg, microseconds */
int multi = 1;		/* third arg, "shared" or "multi" */

bool run = true;
bool debug = true;

int echoport = 8007;	/* avoids needing root */

#define THREADS 50


/* We want these in separate cache lines */
/* FIXME: Need alignment. */
typedef struct worker_data worker;
struct worker_data {
  uint64_t packets;
  uint64_t bytes;
  pthread_t thread;
  char padding[64-24];
};
struct worker_data workers[THREADS];

static void bailout(char* msg)
{
  int saverrr = errno;
  char timetxt[100];
  time_t nowbin;
  struct tm nowstruct;
  time(&nowbin);
  localtime_r(&nowbin, &nowstruct);
  strftime(timetxt, sizeof(timetxt), "%Y-%b-%d %H:%M", &nowstruct);
  printf("** %s %s: errno = %d, %s\n", timetxt, msg, saverrr, strerror(saverrr));
  fflush(stdout);
  sleep(10);  /* Beware of loops in scripts. */
  exit(1);
}

static void catchINT(int sig, siginfo_t *si, void *u) {
  run = false;
}

static void setCatcher(void) {
  int ec;
  struct sigaction vec;

  memset(&vec, '\0', sizeof(vec));
  sigemptyset(&vec.sa_mask);
  vec.sa_sigaction = catchINT;
  vec.sa_flags = SA_SIGINFO;

  ec = sigaction(SIGINT, &vec, NULL);
  if (-1 == ec) bailout("sigaction");

}

static void spinner (int micros) {
  struct timespec start, finish;
  float wall = 0;
  clock_gettime(CLOCK_REALTIME, &start);
  while (wall*1E6 < micros) {
    clock_gettime(CLOCK_REALTIME, &finish);
    wall = (finish.tv_sec-start.tv_sec) +
           (finish.tv_nsec-start.tv_nsec)/1E9;
  }
}

static int make_socket (void) {
  struct sockaddr_in server;
  int sock;
  int ec;
  int one = 1;
  static int global_sock = 0;

  if (global_sock > 0) return global_sock;

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) bailout("socket");


  if (multi) {
#ifdef SO_REUSEPORT_LB
    ec = setsockopt(sock, SOL_SOCKET, SO_REUSEPORT_LB, &one, sizeof(one));
#else
    ec = setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &one, sizeof(one));
#endif
    if (ec < 0) bailout("setsockopt");
  }
    
  bzero((char *)&server, sizeof(server));
  server.sin_family      = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port        = htons(echoport);
  ec = bind(sock, (struct sockaddr *)&server, sizeof(server));
  if (ec < 0) bailout("bind");

  if (!multi) global_sock = sock;
  return sock;
}

static void* echoer (void *arg) {
  worker *me = (worker*)arg;
  int sock = make_socket();

  while (run) {
    int ec;
    uint8_t buffer[100000];
    ssize_t bytes;
    struct sockaddr clientAddr;
    socklen_t clientSize = sizeof(clientAddr);

    bytes = recvfrom(sock, &buffer, sizeof(buffer), 0, &clientAddr, &clientSize);
    if (bytes < 0) {
      if (!run) break;
      bailout("recvfrom");
    }

    if (spin) spinner(spin);

    ec = sendto(sock, &buffer, bytes, 0, &clientAddr, clientSize);
    if (ec < 0) bailout("sendto");

    me->packets += 1;
    me->bytes += bytes;

  }
  return NULL;
}

int main (int argc, char *argv[])
{
 
  uint64_t seen_packets[THREADS];
  uint64_t seen_bytes[THREADS];
  struct timespec start, finish;
  struct timeval start_s, finish_s;
  struct timeval start_u, finish_u;
  struct rusage usage;
  double wall, system, user;
  int ec;

  printf("## worker is %ld bytes.\n", (long)sizeof(worker));
  printf("## workers[0] is %lx.\n", (long int)&workers[0]);
  printf("## workers[1] is %lx.\n", (long int)&workers[1]);

  if (argc > 3) {
    if      (0 == strcmp(argv[3], "multi")) multi = 1;
    else if (0 == strcmp(argv[3], "shared")) multi = 0;
    else bailout("Expected ""multi"" or ""shared"".");
  }
  if (argc > 2) spin = atoi(argv[2]);
  if (spin < 0 || spin > 10000) bailout("Bad spin");
  if (argc > 1) threads = atoi(argv[1]);
  if (threads < 1 || threads > THREADS) bailout("Bad threads");

  signal(SIGPIPE, SIG_IGN);
  setCatcher();
  if (!debug) {
    ec = daemon(1, 1);
    if (ec < 0) bailout("daemon");
  }

  printf("Echo server: %d threads, spin for %d uSec", threads, spin);
  if (multi) {
    if (threads > 1) printf(", using multiple sockets");
  } else {
    if (threads > 1) printf(", sharing a socket");
  }
  printf("\n");
  fflush(stdout);

  clock_gettime(CLOCK_REALTIME, &start);
  getrusage(RUSAGE_SELF, &usage);
  start_u = usage.ru_utime;
  start_s = usage.ru_stime;

  for (int i = 0; i<threads; i++) {
    cpu_set_t cpus;
    int cpu;
    worker *me = &workers[i];
    me->packets = 0;
    me->bytes = 0;
    seen_packets[i] = 0;
    seen_bytes[i] = 0;
    cpu = i;
    if (cpu >= 8) cpu += 8;	/* hack for sam: 0-7, 16-23 */
    if (cpu == 2) cpu++;	/* ksoftirqd does eth0 on dick */
    if (cpu == 21) cpu++;	/* ksoftirqd does eth0 on sam */
    if (cpu == 24) cpu = 2;	/* sam with 15 worker threads */
    CPU_ZERO(&cpus);
    CPU_SET(cpu, &cpus);
    pthread_create(&me->thread, NULL, echoer, me);
    pthread_setaffinity_np(me->thread, sizeof(cpus), &cpus);
  }

  while (run) {
    uint64_t packets, bytes, wire;
    int running;
    char c;

    /* seems to start in readline mode -- use return */
    /* remote stuff doesn't signal on control-C */
    c = getchar();
    if (c == '\03') run = false;

    clock_gettime(CLOCK_REALTIME, &finish);
    getrusage(RUSAGE_SELF, &usage);
    finish_u = usage.ru_utime;
    finish_s = usage.ru_stime;
    wall = (finish.tv_sec-start.tv_sec) +
           (finish.tv_nsec-start.tv_nsec)/1E9;
    system = (finish_s.tv_sec-start_s.tv_sec) +
           (finish_s.tv_usec-start_s.tv_usec)/1E6;
    user = (finish_u.tv_sec-start_u.tv_sec) +
         (finish_u.tv_usec-start_u.tv_usec)/1E6;

    if (!run) printf("\n");  /* avoid ^C on screen */
    packets = bytes = 0;
    running = 0;
    for (int i=0; i<threads; i++) {
      uint64_t grab_packets = workers[i].packets;
      uint64_t grab_bytes = workers[i].bytes;
      uint64_t new_packets = grab_packets-seen_packets[i];
      uint64_t new_bytes = grab_bytes-seen_bytes[i];
       if (0 && threads > 1) printf("## %3d: %8llu %8llu\n", \
           i, (long long unsigned)new_packets, (long long unsigned)new_bytes);
      seen_packets[i] = grab_packets;
      seen_bytes[i] = grab_bytes;
      packets += new_packets;
      bytes += new_bytes;
      if (new_packets) running++;
    }
    wire = bytes + (14+20+8+4)*packets;

    printf("Echoed %lld packets, %lld bytes in %.3f seconds\n",
        (long long)packets, (long long)bytes, wall);
    if (packets)
      printf("Wall: %d packets/second, %.1f uSec/pkt, %d threads\n",
        (int)(packets/wall), wall*1E6/packets, running);
    printf("CPU: %.3f usr, %.3f sys", user, system);
    printf(", %.0f%%", (user+system)*100/wall);
    if (packets)
      printf(", %.1f uSec/pkt", (user+system)*1E6/packets);
    printf("\n");
    if (packets) {
      printf("Wire: %.3f megabits/sec, UDP: %.3f megabits/sec.\n",
        (wire*8.0/wall)/1E6, (bytes*8.0/wall)/1E6);
    }
    fflush(stdout);

    start = finish;
    start_u = finish_u;
    start_s = finish_s;

  }

  return 0;
}
