/*  Last modified on Sat Jan  6 00:11:51 PST 2001 by murray  */

#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>

char *hostname = "localhost";	/* first arg */
int packets = 1000000;		/* second arg */
int bytes = 48;			/* third arg, 48 for NTP, 232 for NTS */
int batch = 10;			/* fourth arg */
int dally = 1000;		/* fifth arg, micro seconds */

int echoport = 8007;

bool run = true;

int sock;
uint8_t send_buf[100000], recv_buf[100000];


static void bailout(char *msg)
{
  int saverrr = errno;
  char timetxt[100];
  time_t nowbin;
  struct tm nowstruct;
  time(&nowbin);
  localtime_r(&nowbin, &nowstruct);
  strftime(timetxt, sizeof(timetxt), "%Y-%b-%d %H:%M", &nowstruct);
  printf("** %s %s: errno = %d, %s\n",
    timetxt, msg, saverrr, strerror(saverrr));
  sleep(10);  /* Beware of loops in scripts. */
  exit(1);
};

static void catchINT(int sig, siginfo_t *si, void *u) {
  run = false;
}

static void setCatcher(void) {
  int ec;
  struct sigaction vec;

  memset(&vec, '\0', sizeof(vec));
  sigemptyset(&vec.sa_mask);
  vec.sa_sigaction = catchINT;
  vec.sa_flags = SA_SIGINFO;

  ec = sigaction(SIGINT, &vec, NULL);
  if (-1 == ec) bailout("sigaction");
}

int main (int argc, char *argv[])
{
  struct hostent *target;
  struct sockaddr_in server, client;
  struct timeval tv, start, finish;
  struct timeval start_s, finish_s;
  struct timeval start_u, finish_u;
  struct rusage usage;
  double wall, system, user;
  float timeout = 2.0;
  float totalBytes;
  int addr;
  int i, ec, len;

  if (1 == argc || 6 < argc) {
    printf("usage: ./send hostname [packets [bytes [batch [dally]]]]\n");
    if (6 < argc) bailout("Too many args");
    exit(1);
  }

  if (argc > 5) dally = atoi(argv[5]);
  if (dally < 0) bailout("Bad dally");

  if (argc > 4) batch = atoi(argv[4]);
  if (batch < 0) bailout("Bad batch");

  if (argc > 3) bytes = atoi(argv[3]);
  if (bytes < 0) bailout("Bad bytes");

  if (argc > 2) packets = atoi(argv[2]);
  if (packets <= 0) bailout("Bad packet count");

  if (argc > 1) hostname = argv[1];
  target = gethostbyname(hostname);
  if (target == NULL) bailout("Bad hostname");

  setCatcher();

  bcopy((char *)target->h_addr, (char *)&addr, sizeof(addr));
  addr = ntohl(addr);
  printf("Sending %d byte packets to %s=>%d.%d.%d.%d\n",
    bytes,
    hostname,
    (addr >> 24) & 0xff,
    (addr >> 16) & 0xff,
    (addr >> 8) & 0xff,
    (addr >> 0) & 0xff);
  if (batch && dally) printf("Dally %d microseconds after every %d packets.\n",
         dally, batch);

  bzero((char *)&server, sizeof(server));
  bcopy((char *)target->h_addr, (char *)&server.sin_addr, target->h_length);
  server.sin_family = target->h_addrtype;
  server.sin_port = htons(echoport);

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (sock < 0) bailout("socket");

  bzero((char *)&client, sizeof(client));
  client.sin_family = AF_INET;
  client.sin_addr.s_addr = htonl(INADDR_ANY);
  client.sin_port = htons(0);
  
  ec = bind(sock, (struct sockaddr *)&client, sizeof(client) );
  if (ec) bailout("bind");
  
  ec = connect(sock, (struct sockaddr *)&server, sizeof(server) );
  if (ec) bailout("connect");

  tv.tv_sec = timeout;
  tv.tv_usec = (timeout-tv.tv_sec)*1E6;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  gettimeofday(&start, NULL);
  getrusage(RUSAGE_SELF, &usage);
  start_u = usage.ru_utime;
  start_s = usage.ru_stime;

  for (i = 0; i < packets && run; i++) {
    len = send(sock, send_buf, bytes, 0);
    if (len != bytes) {
	if (!run) break;
	bailout("send");
    }
    if (batch && dally) {
      if (((i+1)%batch) == 0) usleep(dally);
    }
  }
  packets = i;			/* might be off by 1 */

  gettimeofday(&finish, NULL);
  getrusage(RUSAGE_SELF, &usage);
  finish_u = usage.ru_utime;
  finish_s = usage.ru_stime;

  wall = (finish.tv_sec-start.tv_sec) +
         (finish.tv_usec-start.tv_usec)/1E6;
  system = (finish_s.tv_sec-start_s.tv_sec) +
         (finish_s.tv_usec-start_s.tv_usec)/1E6;
  user = (finish_u.tv_sec-start_u.tv_sec) +
         (finish_u.tv_usec-start_u.tv_usec)/1E6;

  if (!run) printf("\n");	/* ^C */
  printf("Sent %d pkts in %.3f sec.\n", packets, wall);
  printf("Wall: %.3f pkts/sec, %.3f us/pkt.\n",
    packets/wall, wall*1E6/packets);
  printf("CPU: %.3f usr, %.3f sys, %.0f%%, %.1f uSec/pkt.\n",
    user, system, (user+system)*100/wall, (user+system)*1E6/packets);  
  totalBytes = (14+20+8+bytes+4) *1.0 * packets;
  printf("Wire: %.3f megabits/sec, UDP: %.3f megabits/sec.\n",
    (totalBytes*8/wall)/1E6, (bytes*1.0*packets*8/wall)/1E6);

  close(sock);
  
  return 0;
}
