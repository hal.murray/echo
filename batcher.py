#!/usr/bin/env python3
# https://docs.python.org/3/library/subprocess.html

# This grabs data from both client and server.
# The sequence is:
#   Start server
#   Start client with lots of packets
#   Poke server to reset counters
#   Wait for the server to collect some data
#   Poke server, grab data
#   Kick client, grab data
#   The server can be used for multiple client runs

import signal
import socket
import subprocess
import sys
import time

server_host = "glypnod" # first arg
threads = 1             # second arg, threads on server
nClients = 20           # third arg, clients
spin = 0                # fourth arg, uSec on server
length = 48             # fifth arg, packet length
multi = "multi"         # sixth arg: "multi" or "shared"
server_prog = "./toys/echo/echo-server"
extra = 50              # scan through list

client_hosts = [
  # glypnod, fed3, and ted3 have 8 CPUs, others have 4
  # sam has 16 on each socket
  "glypnod", "fed3", "ted3", "glypnod", "fed3", "ted3", "fed", "dick", "hgm"]

packets = 1000000000

debug = 0

if len(sys.argv) > 7:
  server_prog = "./toys/echo/pecho-server.py"
if len(sys.argv) > 6:
  multi = sys.argv[6]
if len(sys.argv) > 5:
  length = int(sys.argv[5])
if len(sys.argv) > 4:
  spin = int(sys.argv[4])
if len(sys.argv) > 3:
  nClients = int(sys.argv[3])
if len(sys.argv) > 2:
  threads = int(sys.argv[2])
if len(sys.argv) > 1:
  server_host = sys.argv[1]

def start_client (client_host, packets, extra, length):
  arg = "/usr/bin/ssh"
  arg += " " + client_host
  arg += " " + "./toys/echo/echo-client"
  arg += " " + server_host
  arg += " " + str(packets)
  arg += " " + str(extra)
  arg += " " + str(length)
  if debug: print("Running", arg)
  client = subprocess.Popen(
           arg.split(),
           stdin=subprocess.PIPE, stdout=subprocess.PIPE,
           stderr=subprocess.DEVNULL, text=True)
  client.client_host = client_host
  return client

def stop_client (client):
  if debug: print("Killing client...")
  try:
    # client.stdin.write("\x03")
    client.stdin.write("\n")
    client.stdin.flush()
  except BrokenPipeError:
    # get here if client bails
    ans = client.stdout.read()
    if ans.find("Lost pkt:") != -1:
      global lost, broken
      lost += 1
      broken.append(client.client_host)
      # print("## Broken Pipe:", client.client_host)
      return
    print("## Broken Pipe:", client.client_host, "=>", ans)
    return
  # client.send_signal(signal.SIGINT)  # works for local client
  time.sleep(0.1)
  t0 = time.time()
  ans = client.stdout.read()
  t1 = time.time()
  if (t1-t0) > 1.0:
    print("## client: %.1f, %s" % ((t1-t0), client.client_host))
    print(ans, end="")
  if 0 or debug: print("##", ans)
  if debug: print("## end")

## Sample client:
#  Sending 48 byte packets to glypnod=>192.168.1.3
#  With 10 extra packets in flight.
#  ^C
#  Exchanged 279466 pkts in 9.346 sec.
#  Wall: 29903 pkts/sec, 33.4 us/pkt.
#  CPU: 0.362 usr, 2.160 sys, 27%, 9.0 uSec/pkt.
#  Wire: 22.487 megabits/sec, UDP: 11.483 megabits/sec.
  if debug: print("## 4 ans:", ans)
  lines = ans.splitlines()
  # print(lines)
  if ans.find("**") != -1:
    if debug: print("##", ans)
    sys.stdout.flush()
    return
  if ans.find("Lost:") != -1:
    # this happens with 50 extras and 5000 byte packets
    if debug: print("##", ans)
    sys.stdout.flush()
    return
  # We drop printout from client
  return

# get rid of old server process that we didn't clean up
ans = subprocess.getoutput("/usr/bin/ssh " + server_host + " killall echo-server")
if debug: print("Kill:", ans)

def start_server (threads, spin, multi):
  # setup server on the server_host system
  server = subprocess.Popen(
           ["/usr/bin/ssh", server_host,
               server_prog, str(threads), str(spin), multi],
           stdin=subprocess.PIPE, stdout=subprocess.PIPE,
           stderr=subprocess.DEVNULL, text=True)
  time.sleep(1.0)
  if debug: print("1:", server.stdout.readline()[:-1])
  server.stdin.write("\n")
  server.stdin.flush()
  while True:
    ans = server.stdout.readline()[:-1]
    # if ans.find("Echo server") == 0: print("#", ans)
    if debug: print("1 srv:", ans)
    if ans.find("CPU:") == 0: break
  return server

def stop_server (server):
  # print("# Killing server...")
  server.stdin.write("\x03\04")     # ^D needed for python
  server.stdin.flush()
  return

# 48 is simple NTP
# 68 is NTP plus AES shared key
# 232 is NTP plus NTS
# 340, 448, 556, ..., 988 are extra cookies
lengths = [0,
    1, 2, 5, 10, 20, 48, 68,
    100, 232, 340, 448, 556, 988, 1200,
    2000, 5000,
    10000, 20000, 50000]

extras = [
    0, 1, 2, 3, 4, 5, 7, 10, 12, 15,
    20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75]

spins = [0, 1, 2, 3, 4, 5, 7, 10, 20, 30, 40, 50, 50, 50, 50, 50]

print("#", time.asctime())
# get remote coalesce delay
ans = subprocess.getoutput("/usr/bin/ssh " + \
  server_host + " /usr/sbin/ethtool -c eth0 | grep rx-usecs:")
# Argh, crap from ssh warnings is an extra line
ans = ans.split("\n")[-1]
rxusec = ans.split()[1]
print("# server=%s, threads=%d, spin=%d, clients=%d, length=%d" % \
  (server_host, threads, spin, nClients, length))
print("#", server_prog, multi, "rx-usecs=%s" % rxusec)

## print("# Echoing %s=>%s" % (client_host, server_host))
print("# pkts/s  megabits/s   uSec/pkt   CPU")
print("#         wire   UDP  wall     server thr spn ext lng")

sys.stdout.flush()

## for length in lengths:
for extra in extras:
## for spin in spins:

  # if extra and length > 1500: break     # fragmentation, lost packets
  if extras*clients > 500: break

  server =  start_server(threads, spin, multi)

  clients = []
  next = []
  while len(clients) < nClients:
    if next == []: next = client_hosts
    host = next[0]
    next = next[1:]
    if host == server_host: continue
    client = start_client (host, packets, extra, length)
    clients.append(client)
    if debug: print("Started", client.client_host)

  time.sleep(1)     # let things get started
  #poke server, drop answers
  server.stdin.write("\n")
  server.stdin.flush()
  while True:
    ans = server.stdout.readline()[:-1]
    if debug: print("2 srv:", ans)
    if ans.find("Wire:") == 0: break
  time.sleep(5)     # let server collect data

## Sample server:
#  Echoed 687668 packets, 14873020 bytes in 59.977 seconds
#  Wall: 11465 packets/second, 87.2 uSec/pkt, 1 threads
#  CPU: 0.463 usr, 1.079 sys, 3%, 2.2 uSec/pkt
#  Wire: 6.203 megabits/sec, UDP: 1.984 megabits/sec.

  #poke server, keep answers
  server.stdin.write("\n")
  server.stdin.flush()
  while True:
    srv = server.stdout.readline()[:-1]
    if debug: print("## 3 srv:", srv)
    if srv.find("Wall:") == 0: srv_wall = srv
    if srv.find("CPU:") == 0: srv_cpu = srv
    if srv.find("Wire:") == 0: srv_wire = srv; break

  lost = 0
  broken = []
  start = time.time()
  for client in clients:
    if debug: print("Stopping", client.client_host)
    stop_client(client)
  stop = time.time()

## Wall: 5183 packets/second, 192.9 uSec/pkt, 7 threads
## CPU: 0.052 usr, 0.388 sys, 9%, 17.0 uSec/pkt
## Wire: 3.898 megabits/sec, UDP: 1.990 megabits/sec.
  wall = srv_wall.split()
  pktsPerSec = float(wall[1])
  wallPerPkt = float(wall[3])
  activeThreads = int(wall[5])
  cpu = srv_cpu.split()
  cpuPerPkt = float(cpu[6])
  cpuPerCent = int(cpu[5][:-2])   # drop "%,"
  wire = srv_wire.split()
  wireBW = float(wire[1])
  udpBW = float(wire[4])
  print("%8.0f %5.1f %5.1f %5.1f %4.1f %4d%% %3d %3d %3d %3d" %
      (pktsPerSec, wireBW, udpBW, wallPerPkt,
       cpuPerPkt, cpuPerCent,
       activeThreads, spin, extra, length), end="")
  if (stop-start) > 0.2*nClients:
    print(" %d %.2f" % (lost, (stop-start)), end="")
  else:
    if lost: print(" %d" % lost, end="")
  if broken: print(" ", " ".join(broken), end="")
  print()
  sys.stdout.flush()

  if debug: print()

  stop_server(server)

print("#", time.asctime())

