#!/usr/bin/env python3

# https://docs.python.org/3/library/subprocess.html


import resource
import signal
import socket
import sys
import threading
import time

threads = 1             # first arg
spin = 0                # second arg
multi = 0               # third arg: multi or shared

echoport = 8007;        #  avoids needing root

run = True

workers = {}

if len(sys.argv) > 3:
  if   "shared" == sys.argv[3]: multi = 0
  elif  "multi" == sys.argv[3]: multi = 1
  else:
    print("## Expected ""multi"" or ""shared"".")
    sys.exit(1)
if len(sys.argv) > 2:
  spin = int(sys.argv[2])
if len(sys.argv) > 1:
  threads = int(sys.argv[1])

class worker:
  def __init__(self):
    self.packets = 0
    self.bytes = 0
    self.thread = None
    self.seen_packets = 0
    self.seen_bytes = 0

def catchINT(num, frame):
  global run
  print("Caught one", run)
  run = False

def setCatcher():
  signal.signal(signal.SIGINT, catchINT);

def spinner(micros):
  start = time.time()
  wall = 0
  while (wall*1E6 < micros):
    finish = time.time()
    wall = finish-start

global_sock = None
def make_socket():
  global global_sock
  if global_sock:
    return global_sock
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  if multi:
    # FreeBSD needs SO_REUSEPORT_LB
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
  sock.bind(('', echoport))
  if not multi:
    global_sock = sock
  return sock

def echoer(arg):
  sock = make_socket()
  while run:
    (data, addr) = sock.recvfrom(100000)
    arg.packets += 1
    arg.bytes += len(data)

    if (spin): spinner(spin)

    sock.sendto(data, addr)

signal.signal(signal.SIGPIPE, signal.SIG_IGN);
setCatcher();

print("Python Echo server: %d threads, spin for %d uSec" % (threads, spin), end="")
if multi:
  if threads > 1: print(", using multiple sockets", end="")
else:
  if threads > 1: print(", sharing a socket", end="")
print();
sys.stdout.flush();

start = time.time()
x=resource.getrusage(resource.RUSAGE_SELF)
start_u = x[0]
start_s = x[1]

for i in range(threads):
  arg = worker()
  workers[i] = arg
  arg.thread = threading.Thread(group=None, target=echoer, args=(arg,), daemon=True)
  arg.thread.start()

while run:

  c = sys.stdin.read(1)
  if c == "\03": print()        # ^C clutter on screen
  if c == "\04": run = False

  x=resource.getrusage(resource.RUSAGE_SELF)
  finish = time.time()
  finish_u = x[0]
  finish_s = x[1]
  wall = finish-start
  user = finish_u - start_u
  system = finish_s - start_s

  if not run: print()       # avoid ^C on screen
  packets = xbytes = 0
  running = 0
  for i in range(threads):
    arg = workers[i]
    grab_packets = arg.packets
    grab_bytes = arg.bytes
    new_packets = grab_packets-arg.seen_packets
    new_bytes = grab_bytes-arg.seen_bytes
    print("%3d, %6d, %9d" % (i, arg.packets, arg.bytes))
    arg.seen_packets = grab_packets
    arg.seen_bytes = grab_bytes
    packets += new_packets
    xbytes += new_bytes
    if new_packets: running += 1
  wire = xbytes + (14+20+8+4)*packets

  print("Echoed %d packets, %d bytes in %.3f seconds" %
        (packets, xbytes, wall))
  if packets:
    print("Wall: %d packets/second, %.1f uSec/pkt, %d threads" %
      (packets/wall, wall*1E6/packets, running))
  print("CPU: %.3f usr, %.3f sys" % (user, system), end="")
  print(", %.0f%%" % ((user+system)*100/wall), end="")
  if packets:
    print(", %.1f uSec/pkt" % ((user+system)*1E6/packets), end="")
  print()
  if packets:
    print("Wire: %.3f megabits/sec, UDP: %.3f megabits/sec." %
      ((wire*8.0/wall)/1E6, (xbytes*8.0/wall)/1E6))
  sys.stdout.flush();



# hangs if we don't join them
# but they are waiting for a packet
# no way to kill a thread or exit while it is still running
# daemon above let's us exit without joining
for i in range(threads):
  arg = workers[i]
  # arg.thread.join()

