/*  Last modified on Sat Jan  6 00:11:51 PST 2001 by murray  */

#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>

char *hostname = "localhost";	/* first arg */
int packets = 100000;		/* second arg */
int extra = 0;			/* third arg */
int bytes = 48;			/* fourth arg, 48 for NTP, 232 for NTS */
int sockets = 10;		/* fifth arg */
int clump = 1;			/* sixth arg */
int spin = 0;			/* seventh arg, microseconds */

int echoport = 8007;

bool run = true;

#define NSOCKETS 250
int socks[NSOCKETS];
uint32_t send_buf[20000], recv_buf[20000];


static void bailout(char *msg)
{
  int saverrr = errno;
  char timetxt[100];
  time_t nowbin;
  struct tm nowstruct;
  time(&nowbin);
  localtime_r(&nowbin, &nowstruct);
  strftime(timetxt, sizeof(timetxt), "%Y-%b-%d %H:%M", &nowstruct);
  printf("** %s %s: errno = %d, %s\n",
    timetxt, msg, saverrr, strerror(saverrr));
  sleep(10);  /* Beware of loops in scripts. */
  exit(1);
};

static void catchINT(int sig, siginfo_t *si, void *u) {
  run = false;
}

static void setCatcher(void) {
  int ec;
  struct sigaction vec;

  memset(&vec, '\0', sizeof(vec));
  sigemptyset(&vec.sa_mask);
  vec.sa_sigaction = catchINT;
  vec.sa_flags = SA_SIGINFO;

  ec = sigaction(SIGINT, &vec, NULL);
  if (-1 == ec) bailout("sigaction");
}

static void* watcher (void *arg) {
  getchar();
  run = false;
  return NULL;
}

static void spinner (int micros) {
  struct timespec start, finish;
  float wall = 0;
  clock_gettime(CLOCK_REALTIME, &start);
  while (wall*1E6 < micros) {
    clock_gettime(CLOCK_REALTIME, &finish);
    wall = (finish.tv_sec-start.tv_sec) +
           (finish.tv_nsec-start.tv_nsec)/1E9;
  }
}

static int make_socket(struct hostent *target) {
  struct sockaddr_in server, client;
  struct timeval tv;
  float timeout = 2.0;
  int sock, ec;

  bzero((char *)&server, sizeof(server));
  bcopy((char *)target->h_addr, (char *)&server.sin_addr, target->h_length);
  server.sin_family = target->h_addrtype;
  server.sin_port = htons(echoport);

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (sock < 0) bailout("socket");

  bzero((char *)&client, sizeof(client));
  client.sin_family = AF_INET;
  client.sin_addr.s_addr = htonl(INADDR_ANY);
  client.sin_port = htons(0);
  
  ec = bind(sock, (struct sockaddr *)&client, sizeof(client) );
  if (ec) bailout("bind");
  
  ec = connect(sock, (struct sockaddr *)&server, sizeof(server) );
  if (ec) bailout("connect");

  tv.tv_sec = (int)timeout;
  tv.tv_usec = (int)((timeout-tv.tv_sec)*1E6);
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  if (1) {
    const int on = 1;
#ifdef SO_TIMESTAMPNS
    setsockopt(sock, SOL_SOCKET, SO_TIMESTAMPNS, &on, sizeof(on));
#else
    setsockopt(sock, SOL_SOCKET, SO_TIMESTAMP, &on, sizeof(on));
#endif
  }

  return sock;
}

int main (int argc, char *argv[])
{
  pthread_t junk;
  struct hostent *target;
  struct timeval start, finish;
  struct timeval start_s, finish_s;
  struct timeval start_u, finish_u;
  struct rusage usage;
  double wall, system, user;
  float totalBytes;
  int addr;
  unsigned int socktx=0, sockrx=0;
  unsigned int sendcnt = 0, recvcnt = 0;
  int i, j, len;

  memset(send_buf, 0, sizeof(send_buf));
  memset(recv_buf, 0, sizeof(recv_buf));

  if (argc > 7) spin = atoi(argv[7]);
  if (spin < 0) bailout("Bad spin");

  if (argc > 6) clump = atoi(argv[6]);
  if (clump < 1) bailout("Bad clump");

  if (argc > 5) sockets = atoi(argv[5]);
  if (sockets < 1 || sockets > NSOCKETS) bailout("Bad sockets");

  if (argc > 4) bytes = atoi(argv[4]);
  if (bytes < 0) bailout("Bad bytes");

  if (argc > 3) extra = atoi(argv[3]);
  if (extra < 0) bailout("Bad extra count");

  if (argc > 2) packets = atoi(argv[2]);
  if (packets <= 0) bailout("Bad packet count");

  if (argc > 1) hostname = argv[1];
  target = gethostbyname(hostname);
  if (target == NULL) bailout("Bad hostname");

  setCatcher();
  pthread_create(&junk, NULL, watcher, NULL);

  for (i = 0; i < sockets; i++) {
    socks[i] = make_socket(target);
  }

  bcopy((char *)target->h_addr, (char *)&addr, sizeof(addr));
  addr = ntohl(addr);
  printf("Sending %d byte packets to %s=>%d.%d.%d.%d, using %d sockets.\n",
    bytes,
    hostname,
    (addr >> 24) & 0xff,
    (addr >> 16) & 0xff,
    (addr >> 8) & 0xff,
    (addr >> 0) & 0xff,
    sockets);
  if (extra) printf("With %d extra packets in flight.\n", extra);
  if (spin) printf("Spin for %d microseconds after every %d packets.\n", \
	spin, clump);

  for (i = 0; i < extra; i++) {
    len = send(socks[socktx], send_buf, bytes, 0);
    if (len != bytes) bailout("send-extra");
    socktx = (socktx+1) % sockets;
    sendcnt++;
    send_buf[0]++;
    usleep(10);
  }

  gettimeofday(&start, NULL);
  getrusage(RUSAGE_SELF, &usage);
  start_u = usage.ru_utime;
  start_s = usage.ru_stime;

  for (i = 0; i < packets && run;) {
    for (j = 0; j < clump && run; j++, i++) {
      len = send(socks[socktx], send_buf, bytes, 0);
      if (!run) break;
      if (len != bytes) bailout("send");
      socktx = (socktx+1) % sockets;
      sendcnt++;
      send_buf[0]++;
      if (clump > 3) usleep(2);
    }
    for (j = 0; j < clump && run; j++) {
      len = recv(socks[sockrx], recv_buf, sizeof(recv_buf), 0);
      if (!run) break;		/* extra(s) sent */
      if (len != bytes) {
	printf("Lost pkt: i %d, j %d, send %d, recv %d\n",
		i, j, sendcnt, recvcnt);
	bailout("recv");
      }
      sockrx = (sockrx+1) % sockets;
      recvcnt++;
    }
    if (spin) spinner(spin);
  }
  packets = i;			/* might be off by 1 */

  gettimeofday(&finish, NULL);
  getrusage(RUSAGE_SELF, &usage);
  finish_u = usage.ru_utime;
  finish_s = usage.ru_stime;

  for (i = 0; i < extra && run; i++) {
    len = recv(socks[sockrx], recv_buf, sizeof(recv_buf), 0);
    if (len != bytes) {
      /* EAGAIN on lost packet */
      printf("Lost: recv-extra for pkt %d of %d %d %d\n", \
          i, extra, sendcnt, recvcnt);
      break;
    }
    sockrx = (sockrx+1) % sockets;
    recvcnt++;
  }

  wall = (finish.tv_sec-start.tv_sec) +
         (finish.tv_usec-start.tv_usec)/1E6;
  system = (finish_s.tv_sec-start_s.tv_sec) +
         (finish_s.tv_usec-start_s.tv_usec)/1E6;
  user = (finish_u.tv_sec-start_u.tv_sec) +
         (finish_u.tv_usec-start_u.tv_usec)/1E6;

  if (!run) printf("\n");	/* ^C */
  printf("Exchanged %d pkts in %.3f sec.\n", packets, wall);
  printf("Wall: %.0f pkts/sec, %.1f us/pkt.\n",
    packets/wall, wall*1E6/packets);
  printf("CPU: %.3f usr, %.3f sys, %.0f%%, %.1f uSec/pkt.\n",
    user, system, (user+system)*100/wall, (user+system)*1E6/packets);  
  totalBytes = (14+20+8+bytes+4) *1.0 * packets;
  printf("Wire: %.3f megabits/sec, UDP: %.3f megabits/sec.\n",
    (totalBytes*8/wall)/1E6, (bytes*1.0*packets*8/wall)/1E6);

  for (i = 0; i < sockets; i++) {
    close(socks[i]);
  }
  
  return 0;
}
