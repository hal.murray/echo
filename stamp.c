/* stamp.c: hack to collect and print packet time stamp skew. */
/*  Last modified on Fri Jan  7 01:13:51 PST 2000 by murray  */


#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/net_tstamp.h>

int clump = 1;	/* first arg */
#define CLUMP 20

int sock;

int echoport = 8007;

static void bailout(char* msg)
{
  int saverrr = errno;
  char timetxt[100];
  time_t nowbin;
  struct tm nowstruct;
  time(&nowbin);
  localtime_r(&nowbin, &nowstruct);
  strftime(timetxt, sizeof(timetxt), "%Y-%b-%d %H:%M", &nowstruct);
  printf("** %s %s: errno = %d, %s\n", timetxt, msg, saverrr, strerror(saverrr));
  fflush(stdout);
  sleep(10);  /* Beware of loops in scripts. */
  exit(1);
}


int main (int argc, char *argv[])
{
 
  int err;
  struct sockaddr_in server;
  const int one = 1;
  const int flags = SOF_TIMESTAMPING_RX_HARDWARE | SOF_TIMESTAMPING_RX_SOFTWARE | SOF_TIMESTAMPING_SOFTWARE | SOF_TIMESTAMPING_RAW_HARDWARE;
  uint8_t buffer[100000];
  ssize_t bytes;
  int hits[CLUMP];
  struct timespec wire[CLUMP];
  struct timespec pre_recv[CLUMP];
  struct timespec post_recv[CLUMP];

  if (argc > 2) bailout("too many args");
  if (argc > 1) clump = atoi(argv[1]);
  if (clump < 1 || clump > CLUMP) bailout("Bad clump");

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) bailout("socket");
    
  bzero((char *)&server, sizeof(server));
  server.sin_family      = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port        = htons(echoport);
  err = bind(sock, (struct sockaddr *)&server, sizeof(server));
  if (err < 0) bailout("bind");

  if (0)
    err = setsockopt(sock, SOL_SOCKET, SO_TIMESTAMPNS, &one, sizeof(one));
  else
    err = setsockopt(sock, SOL_SOCKET, SO_TIMESTAMPING, &flags, sizeof(flags));
  if (err < 0) bailout("setsockopt");

  for (int k = 0; ;k++) {
    struct timespec ts0, ts1, ts2;
    for (int i=0; i<clump; i++) {
      struct iovec iovec;
      struct cmsghdr *cmsg;
      struct msghdr msg;
      char control[100];   /* FIXME: Need space for time stamp plus overhead */
      iovec.iov_base        = &buffer;
      iovec.iov_len         = sizeof(buffer);
      memset(&msg, '\0', sizeof(msg));
      msg.msg_name       = NULL;
      msg.msg_namelen    = 0;
      msg.msg_iov        = &iovec;
      msg.msg_iovlen     = 1;
      msg.msg_flags      = 0;
      msg.msg_control    = (void *)&control;
      msg.msg_controllen = sizeof(control);

      clock_gettime(CLOCK_REALTIME, &pre_recv[i]);
      bytes = recvmsg(sock, &msg, 0);
      if (bytes < 0) bailout("recvmsg");
      clock_gettime(CLOCK_REALTIME, &post_recv[i]);

      /* There should be only one cmsg. */
      cmsg = CMSG_FIRSTHDR(&msg);
      if (NULL == cmsg) bailout("no cmsg");
      hits[i] = 0;
      for (; cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
        if (SCM_TIMESTAMPNS != cmsg->cmsg_type) {
          wire[i] = *(struct timespec *)CMSG_DATA(cmsg);
          hits[i] += 1;
        } else if (SCM_TIMESTAMPING != cmsg->cmsg_type) {
          struct timespec *ts = (struct timespec *)CMSG_DATA(cmsg);
          wire[i] = ts[0];
          ts0 = ts[0];
          ts1 = ts[1];
          ts2 = ts[2];
          hits[i] += 2;
        } else {
          printf("## type: %d, level %d, lng %ld\n", 
              cmsg->cmsg_type, cmsg->cmsg_level, cmsg->cmsg_len);
          bailout("unknown cmsg type");
        }
      }

    }
    for (int i=0; i<clump; i++) {
      uint64_t ns0, ns1, ns2;
      ns0 = (wire[i].tv_sec-wire[0].tv_sec)*1000000000;
      ns0 += (wire[i].tv_nsec-wire[0].tv_nsec);
      ns1 = (post_recv[i].tv_sec-wire[i].tv_sec)*1000000000;
      ns1 += (post_recv[i].tv_nsec-wire[i].tv_nsec);
      ns2 = (post_recv[i].tv_sec-pre_recv[i].tv_sec)*1000000000;
      ns2 += (post_recv[i].tv_nsec-pre_recv[i].tv_nsec);
      printf("%2d %6ld %6ld %6ld  %d\n", i, ns0, ns1, ns2, hits[i]);
    }
    printf("ts0: %9ld %9ld\n", ts0.tv_sec, ts0.tv_nsec);
    printf("ts1: %9ld %9ld\n", ts1.tv_sec, ts1.tv_nsec);
    printf("ts2: %9ld %9ld\n", ts2.tv_sec, ts2.tv_nsec);
  }

  close(sock);
  return 0;
}
